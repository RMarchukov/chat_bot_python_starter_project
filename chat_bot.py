from random import shuffle

films = {'thriller': ['Аватар 2: Путь воды',
                      'Человек-Паук: Нет пути домой',
                      'Пираты карибского моря',
                      'Начало',
                      'Джентельмены'
                      ],
         'western': ['Джанго освобожденный',
                     'Быстрый и мертвый',
                     'Зорро',
                     'Омерзительная восьмерка',
                     'Железная хватка'
                     ],
         'detective': ['Достать ножи',
                       'Смерть на ниле',
                       'Игры разумов',
                       'Невидимый гость',
                       'Простая просьба',
                       ],
         'drama': ['Зеленая миля',
                   'Мира',
                   '1+1',
                   'Хатико',
                   'Побег из шоушенка'
                   ],
         'comedy': ['Чего хотят женщины',
                    'Душа',
                    'Операция фортуна',
                    'Пираты карибского моря',
                    'Достуатся до небес'
                    ],
         'horror': ['Чужой',
                    'Клаустрофобы',
                    'Заклятие',
                    'Доктор сон',
                    'Астрал'
                    ]
         }

musics = {'dancing': ['“Something Just Like This”, исп. The Chainsmokers & Coldplay',
                      '“Ghost”, исп. Parekh and Singh',
                      '“Shape of You”, исп. Ed Sheeran',
                      '“Sweet Creature”, исп. Harry Styles'
                      ],
          'electro': ['“Without You”, исп. Avicii',
                      '“Only You”, исп. Selena Gomez',
                      '“Paris”, исп. The Chainsmokers',
                      '“Hypnotised”, исп. Coldplay'
                      ],
          'rok': ['“Time of Our Lives”, исп. James Blunt',
                  '“Sign of the Times”, исп. Harry Styles',
                  '“Goodbye”, исп. Echosmith',
                  '“Malibu”, исп. Miley Cyrus'
                  ],
          'pop': ['“Attention”, исп. Charlie Puth',
                  '“What About Us”, исп. P!nk',
                  '“Praying”, исп. Kesha',
                  '“Chained to the Rhythm”, исп. Katy Perry'
                  ]
          }

anecdotes = [
    'Приемная комиссия в театральном институте. Абитуриентке говорят: — Девушка, а изобразите-ка нам что-нибудь '
    'эротическое, но с обломом в конце. Абитуриентка, не долго думая: — А!.. Ааа!! Ааааа!!! Ааа-а-аа-пчхи!!!',
    'Мужик купил шагомер. За год прошёл 3000 км. За это время ВЫПИЛ 120 л водки. Расход — 4 л на 100 км',
    'Как говорил мой учитель — «Если не получается сегодня, получится завтра. Если не получится завтра, '
    'получишь пиzzюлей».',
    'Новогодняя ночь полна волшебства. Только этой ночью бокал шампанского превращается в три дня беспробудного '
    'пьянства…',
    'Познать, что такое бесконечность, можно только одним единственным способом: попытаться выбить всю пыль из ковра.'
    ]


def show_films(genre):
    if genre in films.keys():
        for film in films[genre]:
            print(film)
    else:
        print("This genre isn't found")


def show_musics(genre):
    if genre in musics.keys():
        for music in musics[genre]:
            print(music)
    else:
        print("This genre isn't found")


def show_anecdote(number):
    try:
        print(anecdotes[number-1])
    except IndexError:
        print('I have only 5 anecdotes)')


def game(item):
    items = ['stone', 'shears', 'paper']
    shuffle(items)
    bot_item = items[0]
    if bot_item == item:
        print(f'bot item: {bot_item} = your item: {item}\n go next...')
    else:
        if bot_item == 'stone' and item == 'shears':
            print(f'bot item: {bot_item} > your item: {item}\n You lost')
        elif bot_item == 'stone' and item == 'paper':
            print(f'bot item: {bot_item} < your item: {item}\n You won')
        elif bot_item == 'shears' and item == 'stone':
            print(f'bot item: {bot_item} < your item: {item}\n You won')
        elif bot_item == 'shears' and item == 'paper':
            print(f'bot item: {bot_item} > your item: {item}\n You lost')
        elif bot_item == 'paper' and item == 'stone':
            print(f'bot item: {bot_item} > your item: {item}\n You lost')
        elif bot_item == 'paper' and item == 'shears':
            print(f'bot item: {bot_item} > your item: {item}\n You won')
        else:
            print("Your item isn't found.")


def play_game():
    item = ''
    while item != 'end':
        item = input('Enter your item ("end" for end): ')
        game(item)


while True:
    per = input('menu:\t1 - show films\n'
                '\t\t2 - show music\n'
                '\t\t3 - show anecdote\n'
                '\t\t4 - play game\n'
                '\t\t5(and another) - off\n\n'
                'Enter what you want: ')
    if per == '1':
        genre = input('Enter genre of films(thriller, western, detective, drama, comedy, horror): ')
        show_films(genre)
    elif per == '2':
        genre = input('Enter genre of music(dancing, electro, rok, pop): ')
        show_musics(genre)
    elif per == '3':
        number = int(input('Enter number of anecdote(1-5): '))
        show_anecdote(number)
    elif per == '4':
        play_game()
    else:
        break
